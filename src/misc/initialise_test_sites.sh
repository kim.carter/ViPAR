** put this in init script 
drop database vipar_test_study;
create database vipar_test_study;
use vipar_test_study;

CREATE TABLE `AUS_test` (
  `vipar_test_study_auto` int(11) NOT NULL AUTO_INCREMENT,
  `age` tinyint(3) DEFAULT NULL,
  `sex` bigint(1) DEFAULT NULL,
  `bmi` decimal(4,2) DEFAULT NULL,
  `eversmoke` bigint(1) DEFAULT NULL,
  PRIMARY KEY (`vipar_test_study_auto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `UK_test` (
  `vipar_test_study_auto` int(11) NOT NULL AUTO_INCREMENT,
  `age` tinyint(3) DEFAULT NULL,
  `sex` bigint(1) DEFAULT NULL,
  `bmi` decimal(4,2) DEFAULT NULL,
  `eversmoke` bigint(1) DEFAULT NULL,
  PRIMARY KEY (`vipar_test_study_auto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `SWE_test` (
  `vipar_test_study_auto` int(11) NOT NULL AUTO_INCREMENT,
  `age` tinyint(3) DEFAULT NULL,
  `sex` bigint(1) DEFAULT NULL,
  `bmi` decimal(4,2) DEFAULT NULL,
  `eversmoke` bigint(1) DEFAULT NULL,
  PRIMARY KEY (`vipar_test_study_auto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

GRANT SELECT on vipar_test_study.* TO `remoteselect`@`localhost` IDENTIFIED BY 'Ceysestosk';
GRANT ALL PRIVILEGES on vipar_test_study.* TO `remoteinsup`@`localhost` IDENTIFIED BY 'Ceysestosk';
flush privileges;


mysqlimport -v -p --ignore-lines=1 --fields-terminated-by=,  --columns='age,sex,bmi,eversmoke' --local --user=remoteinsup --host=127.0.0.1 --port=3333 vipar_test_study  AUS_test.csv
mysqlimport -v -p --ignore-lines=1 --fields-terminated-by=,  --columns='age,sex,bmi,eversmoke' --local --user=remoteinsup --host=127.0.0.1 --port=4444 vipar_test_study  SWE_test.csv
mysqlimport -v -p --ignore-lines=1 --fields-terminated-by=,  --columns='age,sex,bmi,eversmoke' --local --user=remoteinsup --host=127.0.0.1 --port=5555 vipar_test_study  UK_test.csv

