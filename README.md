# New stable release: 1.0.0

Changes: https://gitlab.com/kim.carter/ViPAR/master/Changelog

Documentation: https://gitlab.com/kim.carter/ViPAR/tree/master/doc

## What is ViPAR?

We have created a software platform for the Virtual Pooling and Analysis of Research data (ViPAR), which implements database federation techniques to provide researchers with a web-based platform to analyse datasets housed in disparate locations without the need for permanent central storage. Remote sites manage their own harmonised research datasets in a database hosted at their site, while a central server hosts the data federation component and a secure analysis portal. When an analysis is initiated, requested data are retrieved from each remote site and temporarily pooled into computer memory at the central site. The data are then analysed by statistical software, and on completion, results of the analysis are returned to the user and the virtually pooled data are removed from memory.

ViPAR is a secure, flexible, and powerful analysis platform built on open source technology, which facilitates the sharing of research data while being sympathetic to ethical, privacy, and data ownership issues.

## Key features

  * Analysis of data by virtual data pooling (federation) or disparately located datasets - no data is permanently stored at any location 
  * Secure, managed access to the federated datasets
  * Flexible web-based analyical interface (pre-configured for R, SAS, Stata), that enables all standard analyses to be conducted on the federated data, without the user being able to see individual level data
  * Simple, but powerful framework for centralised data management (including data dictionary creation) and analyses for consortia
  * Open source, built on open source technologies

## How can I try ViPAR? 

### Method 1. Pre-configured virtual machine (recommended)

  * We provide pre-configure Virtual Machine images for VirtualBox and VMWare (available from our group homepage - http://bioinformatics.childhealthresearch.org.au/software/vipar). We provide VM images for both the Federation and Portal components (ie the Master site that coordinates the network), as well as a data contributing site. This method is strongly recommended in the first instance, as it enables you to test ViPAR with minimal technical know-how and setup time required.

### Method 2. Manual setup

  * For those wishing to integrate the ViPAR code into their own custom environment, we provide all of the code for the key federation and portal components here in this git repository. Detailed hardware and software requirements are provided in the full user manual (https://gitlab.com/kim.carter/ViPAR/tree/master/doc). 

## Links

  * Home page: https://gitlab.com/kim.carter/ViPAR
  * Git repository: https://gitlab.com/kim.carter/ViPAR.git
  * Documentation: https://gitlab.com/kim.carter/ViPAR/tree/master/doc
  * Downloads of prebuilt Virtual Machines: http://bioinformatics.childhealthresearch.org.au/software/vipar

## Releases
  * Latest stable release: ViPAR 1.0.0 (April 2015)
  * Next release: planned for mid 2016

## Screenshots
![screenshot](https://gitlab.com/kim.carter/ViPAR/raw/master/screenshot1.png)
![screenshot](https://gitlab.com/kim.carter/ViPAR/raw/master/screenshot2.png)

